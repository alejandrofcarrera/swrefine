//
//  StepSeven.m
//  SemanticWebRefine
//
//  Created by Alejandro Fdez Carrera on 12/12/14.
//  Copyright (c) 2014 Alejandro F. Carrera. All rights reserved.
//

#import "StepSeven.h"

@implementation StepSeven

- (id)init
{
    if(self == nil)
    {
        self = [super init];
    }
    return self;
}

- (id)initWithFrame:(NSRect)frameRect
{
    self = [super initWithFrame:frameRect];
    
    [self addFileLabel];
    [self addFileButton];
        
    return self;
}

- (void)addFileLabel
{
    fileLabel = [[NSTextField alloc] initWithFrame:CGRectMake(
        0, self.frame.size.height * 0.6,
        self.frame.size.width, self.frame.size.height * 0.3)];
    [fileLabel setBezeled:NO];
    [fileLabel setDrawsBackground:NO];
    [fileLabel setEditable:NO];
    [fileLabel setSelectable:NO];
    [fileLabel setTextColor:[NSColor colorWithDeviceRed:
        0.31f green:0.31f blue:0.31f alpha:1]];
    [fileLabel setAlignment:NSCenterTextAlignment];
    [fileLabel setStringValue:@"Semantic Web Life Cycle Completed"];
    [fileLabel setFont:[NSFont fontWithName:@"Helvetica Bold" size:16.0f]];
    [self addSubview:fileLabel];
}

- (void)addFileButton
{
    fileButton = [[NSButton alloc] initWithFrame:CGRectMake(
        0, self.frame.size.height * 0.3,
        self.frame.size.width, self.frame.size.height * 0.25)];
    [fileButton setBordered:NO];
    [fileButton setAlignment:NSCenterTextAlignment];
    [fileButton setFont: [NSFont fontWithName:@"Helvetica" size:16.f]];
    [fileButton setTitle:@"Start new Life Cycle ?"];
    [fileButton setTarget:self];
    [fileButton setAction:@selector(clickButtonSelect:)];
    [self addSubview:fileButton];
}

- (IBAction)clickButtonSelect:(id)sender
{
    [self.delegate showNextStep:1];
}

@end
