//
//  AppDelegate.m
//  SemanticWebRefine
//
//  Created by Alejandro Fdez Carrera on 12/12/14.
//  Copyright (c) 2014 Alejandro F. Carrera. All rights reserved.
//

#import "AppDelegate.h"

@interface AppDelegate ()

@property (weak) IBOutlet NSWindow *window;
@end

@implementation AppDelegate

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification
{

    NSView *view = self.window.contentView;
    
    CATransition *transition = [CATransition animation];
    [transition setType:kCATransitionPush];
    [transition setSubtype:kCATransitionFromRight];
    [view setAnimations:[NSDictionary dictionaryWithObject:transition forKey:@"subviews"]];
    [view setWantsLayer:YES];
    
    float realHeightView = view.bounds.size.height - 20;
    
    // Welcome label
    titleLabel = [[NSTextField alloc] initWithFrame:CGRectMake(0, realHeightView * 0.65,
        view.bounds.size.width, realHeightView * 0.2)];
    [titleLabel setBezeled:NO];
    [titleLabel setDrawsBackground:NO];
    [titleLabel setEditable:NO];
    [titleLabel setSelectable:NO];
    [titleLabel setAlignment:NSCenterTextAlignment];
    [titleLabel setFont: [NSFont fontWithName:@"Helvetica" size:30.f]];
    [titleLabel setStringValue:@"Welcome to Semantic Web Refine \n Interface"];
    [titleLabel setTextColor:[NSColor colorWithCalibratedRed:0.204f
        green:0.322f blue:0.427f alpha:1.00f]];
    [titleLabel setLineBreakMode:NSLineBreakByWordWrapping];
    [view addSubview:titleLabel];
    
    // Icon create
    imageCreate = [[NSImageView alloc] initWithFrame:CGRectMake(view.bounds.size.width * 0.28, realHeightView * 0.4, realHeightView * 0.1, realHeightView *0.1)];
    [imageCreate setImageScaling:NSImageScaleAxesIndependently];
    [imageCreate setImage:[NSImage imageNamed:@"cubes"]];
    [view addSubview:imageCreate];
    
    // Button create
    buttonCreate = [[NSButton alloc] initWithFrame:CGRectMake(view.bounds.size.width * 0.35,
        realHeightView * 0.4, view.bounds.size.width * 0.38, realHeightView * 0.1)];
    [buttonCreate setBordered:NO];
    [buttonCreate setAlignment:NSCenterTextAlignment];
    [buttonCreate setFont: [NSFont fontWithName:@"Helvetica" size:24.f]];
    [buttonCreate setTitle:@"Create new project"];
    [buttonCreate setTarget:self];
    [buttonCreate setAction:@selector(clickButtonCreate:)];
    [view addSubview:buttonCreate];
    
    // Icon import
    imageImport = [[NSImageView alloc] initWithFrame:CGRectMake(view.bounds.size.width * 0.25, realHeightView * 0.2, realHeightView * 0.1, realHeightView *0.1)];
    [imageImport setImageScaling:NSImageScaleAxesIndependently];
    [imageImport setImage:[NSImage imageNamed:@"folder"]];
    [view addSubview:imageImport];
    
    // Button import
    buttonImport = [[NSTextField alloc] initWithFrame:CGRectMake(view.bounds.size.width
        * 0.30, realHeightView * 0.2, view.bounds.size.width * 0.5, realHeightView * 0.1)];
    [buttonImport setBezeled:NO];
    [buttonImport setDrawsBackground:NO];
    [buttonImport setEditable:NO];
    [buttonImport setSelectable:NO];
    [buttonImport setAlignment:NSCenterTextAlignment];
    [buttonImport setFont: [NSFont fontWithName:@"Helvetica" size:24.f]];
    [buttonImport setStringValue:@"Import external project"];
    [buttonImport setTextColor:[NSColor colorWithCalibratedRed:0.204f
        green:0.322f blue:0.427f alpha:1.00f]];
    [view addSubview:buttonImport];
    
}

- (void)applicationWillTerminate:(NSNotification *)aNotification {
    // Insert code here to tear down your application
}

- (void)cleanWindowContentView
{
    [titleLabel removeFromSuperview];
    [imageCreate removeFromSuperview];
    [buttonCreate removeFromSuperview];
    [imageImport removeFromSuperview];
    [buttonImport removeFromSuperview];
    
    titleLabel = nil;
    imageCreate = nil;
    buttonCreate = nil;
    imageImport = nil;
    buttonImport = nil;
}
- (IBAction)clickButtonCreate:(id)button
{
    [self cleanWindowContentView];
    viewOne = [[CommonStep alloc] initWithFrame:[self.window.contentView bounds]];
    [[self.window.contentView animator] addSubview:viewOne];
}

@end
