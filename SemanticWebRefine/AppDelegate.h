//
//  AppDelegate.h
//  SemanticWebRefine
//
//  Created by Alejandro Fdez Carrera on 12/12/14.
//  Copyright (c) 2014 Alejandro F. Carrera. All rights reserved.
//

#import <Cocoa/Cocoa.h>

#import "CommonStep.h"

@interface AppDelegate : NSObject <NSApplicationDelegate> {
    
    NSTextField *titleLabel;
    NSImageView *imageCreate;
    NSImageView *imageImport;
    NSButton *buttonCreate;
    NSTextField *buttonImport;
    
    CommonStep *viewOne;
        
}


@end

