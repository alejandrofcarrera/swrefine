//
//  StepOne.m
//  SemanticWebRefine
//
//  Created by Alejandro Fdez Carrera on 12/12/14.
//  Copyright (c) 2014 Alejandro F. Carrera. All rights reserved.
//

#import "StepOne.h"

@implementation StepOne

- (id)init
{
    if(self == nil)
    {
        self = [super init];
    }
    return self;
}

- (id)initWithFrame:(NSRect)frameRect
{
    self = [super initWithFrame:frameRect];
    [self addFileLabel];
    [self addFileInput];
    [self addFileButton];
    
    NSLog(@"--------------------");
    NSLog(@"Creating new Project:");
    
    return self;
}

- (void)addFileLabel
{
    NSTextField *fileLabel = [[NSTextField alloc] initWithFrame:CGRectMake(
        self.frame.size.width * 0.085, self.frame.size.height * 0.33,
        self.frame.size.width * 0.45, self.frame.size.height * 0.3)];
    [fileLabel setBezeled:NO];
    [fileLabel setDrawsBackground:NO];
    [fileLabel setEditable:NO];
    [fileLabel setSelectable:NO];
    [fileLabel setTextColor:[NSColor colorWithDeviceRed:
        0.31f green:0.31f blue:0.31f alpha:1]];
    [fileLabel setFont:[NSFont fontWithName:@"Helvetica Bold" size:16.0f]];
    [fileLabel setStringValue:@"Type an URL:"];
    [self addSubview:fileLabel];
}

- (void)addFileInput
{
    fileInput = [[NSTextField alloc] initWithFrame:CGRectMake(
        self.frame.size.width * 0.27, self.frame.size.height * 0.4,
        self.frame.size.width * 0.53, self.frame.size.height * 0.25)];
    [fileInput setBezeled:YES];
    [fileInput setDrawsBackground:NO];
    [fileInput setEditable:YES];
    [fileInput setSelectable:YES];
    [fileInput setTextColor:[NSColor colorWithDeviceRed:
        0.31f green:0.31f blue:0.31f alpha:1]];
    [fileInput setFont:[NSFont fontWithName:@"Helvetica" size:14.0f]];
    [fileInput setPlaceholderString:@"http or https with link in csv format"];
    [self addSubview:fileInput];
}

- (void)addFileButton
{
    fileButton = [[NSButton alloc] initWithFrame:CGRectMake(
        self.frame.size.width * 0.81, self.frame.size.height * 0.41,
        self.frame.size.width * 0.15, self.frame.size.height * 0.25)];
    [fileButton setBordered:NO];
    [fileButton setAlignment:NSLeftTextAlignment];
    [fileButton setFont: [NSFont fontWithName:@"Helvetica" size:16.f]];
    [fileButton setTitle:@"Select"];
    [fileButton setTarget:self];
    [fileButton setAction:@selector(clickButtonSelect:)];
    [self addSubview:fileButton];
}

- (IBAction)clickButtonSelect:(id)sender
{
    if(fileInput.stringValue.length > 0)
    {
        if([fileInput.stringValue containsString:@"http"])
        {
            [self downloadFile:fileInput.stringValue];
        }
        else if([fileInput.stringValue isEqualToString:@"TemporalCSV"])
        {
            [self detectCSV];
        }
        else if([fileInput.stringValue isEqualToString:@"Demo311"])
        {
            [self useDemoProject];
        }
        else
        {
            [self showError:@"Invalid URL"];
        }
    }
}

- (void)detectCSV
{
    BOOL file = [[NSFileManager defaultManager] fileExistsAtPath:@"/tmp/SWR.csv" isDirectory:nil];
    if(file)
    {
        [fileButton setEnabled:NO];
        [fileInput setEditable:NO];
        [fileInput setEnabled:NO];
        [fileInput setTextColor:[NSColor lightGrayColor]];
        connectionData = [[NSMutableData alloc] initWithData:[[NSFileManager defaultManager]
            contentsAtPath:@"/tmp/SWR.csv"]];
        [self createRefineProject];
    }
    else
    {
        [self showError:@"There is not SWR CSV File"];
    }
}

- (void)useDemoProject
{
    projectName = @"SWR_DEMO311";
    [self getRefineProject];
}

- (void)downloadFile:(NSString *)path
{
    connectionData = [[NSMutableData alloc] init];
    connection = [[NSURLConnection alloc] initWithRequest:[NSURLRequest
        requestWithURL:[NSURL URLWithString:path]]
        delegate:self startImmediately:YES];
}

- (void)showError:(NSString *)type
{
    
    // Enable input and button
    [fileButton setEnabled:YES];
    [fileInput setEditable:YES];
    [fileInput setEnabled:YES];
    [fileInput setStringValue:@""];
    [fileInput setTextColor:[NSColor colorWithDeviceRed:
        0.31f green:0.31f blue:0.31f alpha:1]];
    
    // Alert with error
    NSAlert *alert = [[NSAlert alloc] init];
    [alert setMessageText:@"Error - SWR"];
    [alert setInformativeText:type];
    [alert addButtonWithTitle:@"OK"];
    [alert runModal];
    
}

- (void)getRefineProject
{
    connectionData = [[NSMutableData alloc] init];
    connection = [[NSURLConnection alloc] initWithRequest:[NSURLRequest
        requestWithURL:[NSURL URLWithString:@"http://localhost:3333/command/core/get-all-project-metadata"]]
        delegate:self startImmediately:YES];
}

- (void)saveRefineProject:(NSString *)identifier
{
    if(identifier < 0)
    {
        [self showError:@"Unable to find Project on Google Refine"];
    }
    else
    {
        [self.delegate saveRefineProject:identifier];
        [self.delegate showNextStep:2];
    }
}

- (void)createRefineProject
{
    // Please wait label
    [fileButton setTitle:@"Please wait"];
    
    // Create Refine Project
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:
        [NSURL URLWithString:@"http://127.0.0.1:3333/command/core/create-project-from-upload"]];
    
    [request setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
    [request setHTTPShouldHandleCookies:NO];
    [request setTimeoutInterval:300];
    [request setHTTPMethod:@"POST"];
    NSString *boundary = @"SWR_18122014";
    
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary];
    [request setValue:contentType forHTTPHeaderField: @"Content-Type"];
    
    NSString *uuidString = [[NSUUID UUID] UUIDString];
    uuidString = [NSString stringWithFormat:@"ProjectSWR_%@", [uuidString substringToIndex:8] ];

    NSMutableData *body = [NSMutableData data];
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=%@\r\n\r\n", @"project-name"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"%@\r\n", uuidString]
        dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=%@\r\n\r\n", @"separator"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"%@\r\n", @","] dataUsingEncoding:NSUTF8StringEncoding]];
    
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=%@; filename=file.csv\r\n", @"project-file"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[@"Content-Type: text/csv\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:connectionData];
    [body appendData:[[NSString stringWithFormat:@"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    
    [body appendData:[[NSString stringWithFormat:@"--%@--\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    
    [request setHTTPBody:body];
    NSString *postLength = [NSString stringWithFormat:@"%d", (int)[body length]];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue currentQueue]
        completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
        if(data.length > 0)
        {
            NSLog(@"Created Google Refine Project: %@", uuidString);
            projectName = uuidString;
            [self getRefineProject];
        }
        else
        {
            [self showError:@"Check Refine Endpoint: 127.0.0.1:3333"];
        }
    }];
}

- (void)connection:(NSURLConnection *)conn didFailWithError:(NSError *)error
{
    [self showError:@"NSURLConnection"];
}

- (void)connection:(NSURLConnection *)conn didReceiveData:(NSData *)data
{
    [connectionData appendData:data];
    if(![conn.currentRequest.URL.path isEqualToString:
         @"/command/core/get-all-project-metadata"])
    {
        [fileButton setTitle:[NSString stringWithFormat:@"%.f%%",
            ((double)connectionData.length/(double)expectedData) * 100]];
    }
}

- (void)connection:(NSURLConnection *)conn didReceiveResponse:(NSURLResponse *)response
{
    if(![conn.currentRequest.URL.path isEqualToString:
        @"/command/core/get-all-project-metadata"])
    {
        NSHTTPURLResponse *resp = (NSHTTPURLResponse *)response;
        if([resp.allHeaderFields[@"Content-Type"] containsString:@"text/comma-separated-values"] ||
           [resp.allHeaderFields[@"Content-Type"] containsString:@"text/csv"] ||
           [resp.allHeaderFields[@"Content-Type"] containsString:@"application/csv"] ||
           [resp.allHeaderFields[@"Content-Type"] containsString:@"application/x-csv"] ||
           [resp.allHeaderFields[@"Content-Type"] containsString:@"application/excel"] ||
           [resp.allHeaderFields[@"Content-Type"] containsString:@"application/vnd.ms-excel"] ||
           [resp.allHeaderFields[@"Content-Type"] containsString:@"application/vnd.msexcel"])
        {
            expectedData = [resp.allHeaderFields[@"Content-Length"] intValue];
            [fileButton setEnabled:NO];
            [fileInput setEditable:NO];
            [fileInput setEnabled:NO];
            [fileInput setTextColor:[NSColor lightGrayColor]];
        }
        else
        {
            [self showError:@"Invalid Format"];
            [connection cancel];
        }
    }
}

- (void)connectionDidFinishLoading:(NSURLConnection *)conn
{
    if([conn.currentRequest.URL.path isEqualToString:
         @"/command/core/get-all-project-metadata"])
    {
        if(connectionData.length > 0)
        {
            NSDictionary *json = [NSJSONSerialization JSONObjectWithData:connectionData options:0 error:nil];
            json = json[@"projects"];
            NSString *value = nil;
            for(NSString *i in json)
            {
                if([json[i][@"name"] isEqualToString:projectName])
                {
                    value = i;
                    break;
                }
            }
            json = nil;
            NSLog(@"Found Google Refine Project: %@ - %@", projectName, value);
            [self saveRefineProject:value];
        }
        else
        {
            [self showError:[NSString stringWithFormat:@"Check Refine Endpoint 127.0.0.1:3333 or project %@", projectName]];
        }
    }
    else
    {
        [self createRefineProject];
    }
}

@end
