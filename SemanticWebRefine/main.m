//
//  main.m
//  SemanticWebRefine
//
//  Created by Alejandro Fdez Carrera on 12/12/14.
//  Copyright (c) 2014 Alejandro F. Carrera. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, const char * argv[]) {
    return NSApplicationMain(argc, argv);
}
