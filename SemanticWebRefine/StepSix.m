//
//  StepSix.m
//  SemanticWebRefine
//
//  Created by Alejandro Fdez Carrera on 12/12/14.
//  Copyright (c) 2014 Alejandro F. Carrera. All rights reserved.
//

#import "StepSix.h"

@implementation StepSix

- (id)init
{
    if(self == nil)
    {
        self = [super init];
    }
    return self;
}

- (id)initWithFrame:(NSRect)frameRect
{
    self = [super initWithFrame:frameRect];
    
    [self addFileLabel];
    [self addFileInput];
    [self addFileButton];
    
    currentStep = 1;
    turtle = [self generateTurtleBase];
    [self showNextVoidStep];
    
    NSLog(@"--------------------");
    NSLog(@"Creating VoID:");
    
    return self;
}

- (void)addFileLabel
{
    fileLabel = [[NSTextField alloc] initWithFrame:CGRectMake(
        self.frame.size.width * 0.1, self.frame.size.height * 0.33,
        self.frame.size.width * 0.45, self.frame.size.height * 0.3)];
    [fileLabel setBezeled:NO];
    [fileLabel setDrawsBackground:NO];
    [fileLabel setEditable:NO];
    [fileLabel setSelectable:NO];
    [fileLabel setTextColor:[NSColor colorWithDeviceRed:
        0.31f green:0.31f blue:0.31f alpha:1]];
    [fileLabel setFont:[NSFont fontWithName:@"Helvetica Bold" size:16.0f]];
    [self addSubview:fileLabel];
}

- (void)addFileInput
{
    fileInput = [[NSTextField alloc] initWithFrame:CGRectMake(
        self.frame.size.width * 0.179, self.frame.size.height * 0.4,
        self.frame.size.width * 0.57, self.frame.size.height * 0.25)];
    [fileInput setBezeled:YES];
    [fileInput setDrawsBackground:NO];
    [fileInput setEditable:YES];
    [fileInput setSelectable:YES];
    [fileInput setTextColor:[NSColor colorWithDeviceRed:
        0.31f green:0.31f blue:0.31f alpha:1]];
    [fileInput setFont:[NSFont fontWithName:@"Helvetica" size:14.0f]];
    [self addSubview:fileInput];
}

- (void)addFileButton
{
    fileButton = [[NSButton alloc] initWithFrame:CGRectMake(
        self.frame.size.width * 0.76, self.frame.size.height * 0.41,
        self.frame.size.width * 0.15, self.frame.size.height * 0.25)];
    [fileButton setBordered:NO];
    [fileButton setAlignment:NSLeftTextAlignment];
    [fileButton setFont: [NSFont fontWithName:@"Helvetica" size:16.f]];
    [fileButton setTitle:@"Add to VoID"];
    [fileButton setTarget:self];
    [fileButton setAction:@selector(clickButtonSelect:)];
    [self addSubview:fileButton];
}

- (void)showNextVoidStep
{
    switch (currentStep) {
        case 1:
            [fileLabel setStringValue:@"Title:"];
            [fileInput setPlaceholderString:@"dcterms:title property"];
            break;
        case 2:
            [fileInput setStringValue:@""];
            [fileInput setPlaceholderString:@"dcterms:description property"];
            [fileInput setFrame:CGRectMake(
                self.frame.size.width * 0.27, self.frame.size.height * 0.4,
                self.frame.size.width * 0.48, self.frame.size.height * 0.25)];
            [fileLabel setStringValue:@"Description:"];
            break;
        case 3:
            [fileInput setStringValue:@""];
            [fileInput setPlaceholderString:@"dcterms:creator property (IRI)"];
            [fileInput setFrame:CGRectMake(
                self.frame.size.width * 0.375, self.frame.size.height * 0.4,
                self.frame.size.width * 0.375, self.frame.size.height * 0.25)];
            [fileLabel setStringValue:@"Creator & Publisher:"];
            break;
        case 4:
            [fileInput setStringValue:@""];
            [fileInput setPlaceholderString:@"dcterms:creator property (IRI)"];
            [fileInput setFrame:CGRectMake(
                self.frame.size.width * 0.225, self.frame.size.height * 0.4,
                self.frame.size.width * 0.52, self.frame.size.height * 0.25)];
            [fileLabel setStringValue:@"Creator:"];
            break;
        case 5:
            [fileInput setStringValue:@""];
            [fileInput setPlaceholderString:@"void:vocabulary property (IRI)"];
            [fileInput setFrame:CGRectMake(
                self.frame.size.width * 0.27, self.frame.size.height * 0.4,
                self.frame.size.width * 0.48, self.frame.size.height * 0.25)];
            [fileLabel setStringValue:@"Vocabulary:"];
            break;
        default:
            break;
    }
}

- (IBAction)clickButtonSelect:(id)sender
{
    if(fileInput.stringValue.length > 0)
    {
        switch (currentStep) {
            case 1:
                NSLog(@"Added dcterms:title - %@", fileInput.stringValue);
                turtle = [turtle stringByAppendingString:[NSString stringWithFormat:@"\tdcterms:title \"%@\";\n",
                    fileInput.stringValue]];
                currentStep++;
                [self showNextVoidStep];
                break;
            case 2:
                NSLog(@"Added dcterms:description - %@", fileInput.stringValue);
                turtle = [turtle stringByAppendingString:[NSString stringWithFormat:@"\tdcterms:description \"%@\";\n",
                    fileInput.stringValue]];
                currentStep++;
                [self showNextVoidStep];
                break;
            case 3:
                if(![fileInput.stringValue containsString:@"http"])
                {
                    [fileInput setStringValue:@""];
                    [fileInput setPlaceholderString:@"Invalid IRI, try again"];
                }
                else
                {
                    NSLog(@"Added dcterms:publisher - %@", fileInput.stringValue);
                    NSLog(@"Added dcterms:creator - %@", fileInput.stringValue);
                    turtle = [turtle stringByAppendingString:[NSString stringWithFormat:@"\tdcterms:publisher <%@>;\n",
                        fileInput.stringValue]];
                    turtle = [turtle stringByAppendingString:[NSString stringWithFormat:@"\tdcterms:creator <%@>",
                        fileInput.stringValue]];
                    [self askNewVoid];
                }
                break;
            case 4:
                if(![fileInput.stringValue containsString:@"http"])
                {
                    [fileInput setStringValue:@""];
                    [fileInput setPlaceholderString:@"Invalid IRI, try again"];
                }
                else
                {
                    NSLog(@"Added dcterms:creator - %@", fileInput.stringValue);
                    turtle = [turtle stringByAppendingString:[NSString stringWithFormat:@"\tdcterms:creator <%@>",
                        fileInput.stringValue]];
                    [self askNewVoid];
                }
                break;
            case 5:
                if(![fileInput.stringValue containsString:@"http"])
                {
                    [fileInput setStringValue:@""];
                    [fileInput setPlaceholderString:@"Invalid IRI, try again"];
                }
                else
                {
                    NSLog(@"Added void:vocabulary - %@", fileInput.stringValue);
                    turtle = [turtle stringByAppendingString:[NSString stringWithFormat:@"\tvoid:vocabulary <%@>",
                        fileInput.stringValue]];
                    [self askNewVoid];
                }
                break;
            default:
                break;
        }
    }
}

- (void)askNewVoid
{
    alert = [[NSAlert alloc] init];
    [alert setMessageText:@"VoID Creator - SWR"];
    NSButton *sAlert;
    if(currentStep < 5)
    {
        sAlert = [alert addButtonWithTitle:@"Add creator"];
        [alert setInformativeText:@"Do you want to add other creator?"];
    }
    else
    {
        sAlert = [alert addButtonWithTitle:@"Add vocabulary"];
        [alert setInformativeText:@"Do you want to add other vocabulary?"];
    }
    [sAlert setTarget:self];
    [sAlert setAction:@selector(chooseNewCreator)];
    NSButton *dAlert = [alert addButtonWithTitle:@"Skip"];
    [dAlert setTarget:self];
    [dAlert setAction:@selector(chooseSkipCreator)];
    [alert runModal];
}

- (void)chooseNewCreator
{
    turtle = [turtle stringByAppendingString:@",\n"];
    if(currentStep == 3)
    {
        currentStep++;
    }
    [self showNextVoidStep];
    [NSApp endSheet:[alert window]];
    alert = nil;
}

- (void)chooseSkipCreator
{
    
    if(currentStep == 3)
    {
        turtle = [turtle stringByAppendingString:@";\n"];
        currentStep += 2;
        [self showNextVoidStep];
    }
    else if(currentStep == 5)
    {
        turtle = [turtle stringByAppendingString:@".\n"];
        
        [[NSFileManager defaultManager] createFileAtPath:@"/tmp/Export-void.ttl" contents:[turtle dataUsingEncoding:NSUTF16StringEncoding] attributes:nil];
        
        NSLog(@"FileManager - Created /tmp/Export-void.ttl");
        
        [self.delegate showNextStep:7];
    }
    else
    {
        turtle = [turtle stringByAppendingString:@";\n"];
        currentStep++;
        [self showNextVoidStep];
    }
    [NSApp endSheet:[alert window]];
    alert = nil;
}

- (NSString *)generateTurtleBase
{
    NSString *b = @"@prefix base: <http:www.semanticweb.org/>\n\
    @prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .\n\
    @prefix owl: <http://www.w3.org/2002/07/owl#> .\n\
    @prefix adms: <http://www.w3.org/ns/adms#> .\n\
    @prefix dcterms: <http://purl.org/dc/terms/> .\n\
    @prefix xsd: <http://www.w3.org/2001/XMLSchema#> .\n\
    @prefix vann: <http://purl.org/vocab/vann/> .\n\
    @prefix foaf: <http://xmlns.com/foaf/0.1/> .\n\
    @prefix void: <http://rdfs.org/ns/void#> .\n\
    @prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#> .\n\n\
    base:void a void:Dataset;\n\
    \tvoid:uriSpace \"http://www.semanticweb.org/\";\n\
    \tdcterms:status <http://purl.org/adms/status/UnderDevelopment>;\n";
    
    NSDateFormatter *dateF = [[NSDateFormatter alloc] init];
    [dateF setDateFormat:@"yyyy-MM-dd"];
    
    b = [b stringByAppendingString:[NSString stringWithFormat:@"\tdcterms:created \"%@\"^^xsd:date;\n",
        [dateF stringFromDate:[NSDate date]]]];
    
    dateF = nil;
    
    return b;
    
}

@end
