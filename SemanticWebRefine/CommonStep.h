//
//  CommonStep.h
//  SemanticWebRefine
//
//  Created by Alejandro Fdez Carrera on 12/12/14.
//  Copyright (c) 2014 Alejandro F. Carrera. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import <QuartzCore/QuartzCore.h>

#import "PCProgressView.h"
#import "StepOne.h"
#import "StepTwo.h"
#import "StepThree.h"
#import "StepFour.h"
#import "StepFive.h"
#import "StepSix.h"
#import "StepSeven.h"

@interface CommonStep : NSView <ShowStepDelegate> {
    
    PCProgressView *progress;
    NSTextField *titleLabel;
    
    float maxProgress;
    NSTimer *timer;
    
    NSTextField *labelOne;
    NSTextField *labelTwo;
    NSTextField *labelThree;
    NSTextField *labelFour;
    NSTextField *labelFive;
    NSTextField *labelSix;

    // Views
    NSView *currentView;
    
    // Refine
    NSString *refineId;
    NSMutableArray *metaRefine;
}

@end
