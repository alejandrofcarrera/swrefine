//
//  CommonStep.m
//  SemanticWebRefine
//
//  Created by Alejandro Fdez Carrera on 12/12/14.
//  Copyright (c) 2014 Alejandro F. Carrera. All rights reserved.
//

#import "CommonStep.h"

@implementation CommonStep

- (id)init
{
    if(self == nil)
    {
        self = [super init];
    }
    return self;
}

- (id)initWithFrame:(NSRect)frameRect
{
    self = [super initWithFrame:frameRect];
    [self loadProgressView:CGRectMake(0, 20, frameRect.size.width,
        frameRect.size.height-20)];
    currentView = nil;
    [self showInternalStep:1];
    return self;
}

- (void)loadProgressView:(NSRect)frame {
    
    // Progress Indicator
    if(progress == nil)
    {
        progress = [[PCProgressView alloc] initWithFrame:CGRectMake(frame.size.width/2-90, frame.size.height*0.30, 180, 180)];
        [progress setProgressLineWidth:5.0f];
        [progress setBackgroundLineWidth:10.0f];
        [progress setProgressLineColor:[NSColor whiteColor] ];
        [progress setBackgroundLineColor:[NSColor colorWithCalibratedRed:0.204f green:0.322f
            blue:0.427f alpha:1.00f]];
        [progress setProgress:0.16];
        [self addSubview:progress];
    }
    
    // Step title
    titleLabel = [[NSTextField alloc] initWithFrame:CGRectMake(0,
        frame.size.height * 0.82, frame.size.width, frame.size.height * 0.1)];
    [titleLabel setBezeled:NO];
    [titleLabel setDrawsBackground:NO];
    [titleLabel setEditable:NO];
    [titleLabel setSelectable:NO];
    [titleLabel setAlignment:NSCenterTextAlignment];
    [titleLabel setFont: [NSFont fontWithName:@"Helvetica" size:30.f]];
    [titleLabel setTextColor:[NSColor colorWithCalibratedRed:0.204f
        green:0.322f blue:0.427f alpha:1.00f]];
    [self addSubview:titleLabel];
    
    // Icon progress
    NSImageView *imageCreate = [[NSImageView alloc] initWithFrame:CGRectMake(frame.size.width/2-50,
        frame.size.height * 0.42, 100, 100)];
    [imageCreate setImageScaling:NSImageScaleAxesIndependently];
    [imageCreate setImage:[NSImage imageNamed:@"cubes_high"]];
    [self addSubview:imageCreate];
    
    [self createLabels:0];
}

- (void)animateProgressView
{
    float currentValue = progress.progress;
    if(currentValue >= maxProgress)
    {
        if([currentView isKindOfClass:[StepSeven class]])
        {
            [timer invalidate];
            currentValue = 1.0;
        }
        else
        {
            currentValue = 0.0;
        }
    }
    else if(currentValue >= 0.38 && currentValue < 0.43)
    {
        if(![currentView isKindOfClass:[StepSeven class]])
        {
            currentValue += 0.25;
        }
        else
        {
            currentValue += 0.01;
        }
    }
    else
    {
        if([currentView isKindOfClass:[StepSix class]])
        {
            currentValue += 0.04;
        }
        else if([currentView isKindOfClass:[StepFive class]])
        {
            currentValue += 0.03;
        }
        else
        {
            currentValue += 0.01;
        }
    }
    [progress setProgress:currentValue animated:YES];
}

- (void)createLabels:(int)highlight
{
    labelOne = [[NSTextField alloc] initWithFrame:CGRectMake(
        self.frame.size.width * 0.5, self.frame.size.height * 0.64,
        self.frame.size.width * 0.5, self.frame.size.height * 0.1)];
    [labelOne setBezeled:NO];
    [labelOne setDrawsBackground:NO];
    [labelOne setEditable:NO];
    [labelOne setSelectable:NO];
    [labelOne setAlignment:NSCenterTextAlignment];
    [labelOne setFont: [NSFont fontWithName:@"Helvetica" size:24.f]];
    [labelOne setTextColor:[NSColor colorWithCalibratedRed:0.204f
        green:0.322f blue:0.427f alpha:1.00f]];
    [labelOne setStringValue:@"Explotation"];
    [self addSubview:labelOne];
    
    labelTwo = [[NSTextField alloc] initWithFrame:CGRectMake(
        self.frame.size.width * 0.54, self.frame.size.height * 0.47,
        self.frame.size.width * 0.5, self.frame.size.height * 0.1)];
    [labelTwo setBezeled:NO];
    [labelTwo setDrawsBackground:NO];
    [labelTwo setEditable:NO];
    [labelTwo setSelectable:NO];
    [labelTwo setAlignment:NSCenterTextAlignment];
    [labelTwo setFont: [NSFont fontWithName:@"Helvetica" size:24.f]];
    [labelTwo setTextColor:[NSColor colorWithCalibratedRed:0.204f
        green:0.322f blue:0.427f alpha:1.00f]];
    [labelTwo setStringValue:@"Specification"];
    [self addSubview:labelTwo];
    
    labelThree = [[NSTextField alloc] initWithFrame:CGRectMake(
        self.frame.size.width * 0.48, self.frame.size.height * 0.30,
        self.frame.size.width * 0.5, self.frame.size.height * 0.1)];
    [labelThree setBezeled:NO];
    [labelThree setDrawsBackground:NO];
    [labelThree setEditable:NO];
    [labelThree setSelectable:NO];
    [labelThree setAlignment:NSCenterTextAlignment];
    [labelThree setFont: [NSFont fontWithName:@"Helvetica" size:24.f]];
    [labelThree setTextColor:[NSColor colorWithCalibratedRed:0.204f
        green:0.322f blue:0.427f alpha:1.00f]];
    [labelThree setStringValue:@"Modelling"];
    [self addSubview:labelThree];
    
    labelFour = [[NSTextField alloc] initWithFrame:CGRectMake(
        self.frame.size.width * 0.01, self.frame.size.height * 0.64,
        self.frame.size.width * 0.5, self.frame.size.height * 0.1)];
    [labelFour setBezeled:NO];
    [labelFour setDrawsBackground:NO];
    [labelFour setEditable:NO];
    [labelFour setSelectable:NO];
    [labelFour setAlignment:NSCenterTextAlignment];
    [labelFour setFont: [NSFont fontWithName:@"Helvetica" size:24.f]];
    [labelFour setTextColor:[NSColor colorWithCalibratedRed:0.204f
        green:0.322f blue:0.427f alpha:1.00f]];
    [labelFour setStringValue:@"Publication"];
    [self addSubview:labelFour];
    
    labelFive = [[NSTextField alloc] initWithFrame:CGRectMake(
        0, self.frame.size.height * 0.47,
        self.frame.size.width * 0.5, self.frame.size.height * 0.1)];
    [labelFive setBezeled:NO];
    [labelFive setDrawsBackground:NO];
    [labelFive setEditable:NO];
    [labelFive setSelectable:NO];
    [labelFive setAlignment:NSCenterTextAlignment];
    [labelFive setFont: [NSFont fontWithName:@"Helvetica" size:24.f]];
    [labelFive setTextColor:[NSColor colorWithCalibratedRed:0.204f
        green:0.322f blue:0.427f alpha:1.00f]];
    [labelFive setStringValue:@"Linking"];
    [self addSubview:labelFive];
    
    labelSix = [[NSTextField alloc] initWithFrame:CGRectMake(
        0, self.frame.size.height * 0.30,
        self.frame.size.width * 0.5, self.frame.size.height * 0.1)];
    [labelSix setBezeled:NO];
    [labelSix setDrawsBackground:NO];
    [labelSix setEditable:NO];
    [labelSix setSelectable:NO];
    [labelSix setAlignment:NSCenterTextAlignment];
    [labelSix setFont: [NSFont fontWithName:@"Helvetica" size:24.f]];
    [labelSix setTextColor:[NSColor colorWithCalibratedRed:0.204f
        green:0.322f blue:0.427f alpha:1.00f]];
    [labelSix setStringValue:@"Generation"];
    [self addSubview:labelSix];
}

- (void)clearCurrentView:(int)backupStep
{
    [NSAnimationContext runAnimationGroup:^(NSAnimationContext *context) {
        [context setDuration: 2.0];
        [context setTimingFunction: [CAMediaTimingFunction
            functionWithName: kCAMediaTimingFunctionEaseOut]];
        [currentView.animator setAlphaValue: 0.0];
    }
    completionHandler:^{
        [currentView removeFromSuperview];
        currentView = nil;
        [self showInternalStep:backupStep];
    }];
}

- (void)showInternalStep:(int)step
{
    switch (step) {
        case 1:
            if([currentView isKindOfClass:[StepSeven class]])
            {
                [timer invalidate];
            }
            
            timer = [NSTimer scheduledTimerWithTimeInterval:.5 target:self
               selector:@selector(animateProgressView) userInfo:nil repeats:YES];
            [titleLabel setStringValue:@"Step One"];
            [labelOne setTextColor:[NSColor colorWithCalibratedRed:
                0.31f green:0.31f blue:0.31f alpha:1]];
            maxProgress = 0.17;
            currentView = [[StepOne alloc] initWithFrame:CGRectMake(0, 0,
                self.frame.size.width, self.frame.size.height * 0.25)];
            [(StepOne *)currentView setDelegate:self];
            [self addSubview:currentView];
            break;
        case 2: {
            [titleLabel setStringValue:@"Step Two"];
            [labelOne setTextColor:[NSColor colorWithCalibratedRed:
                0.204f green:0.322f blue:0.427f alpha:1.00f]];
            [labelTwo setTextColor:[NSColor colorWithCalibratedRed:
                0.31f green:0.31f blue:0.31f alpha:1]];
            maxProgress = 0.28;
            currentView = [[StepTwo alloc] initWithFrame:CGRectMake(0, 0,
                self.frame.size.width, self.frame.size.height * 0.25)];
            [(StepTwo *)currentView setDelegate:self];
            [(StepTwo *)currentView setRefineId:refineId];
            [(StepTwo *)currentView executeGetInformation];
            [self addSubview:currentView];
            break;
        }
        case 3:
            [titleLabel setStringValue:@"Step Three"];
            [labelTwo setTextColor:[NSColor colorWithCalibratedRed:
                0.204f green:0.322f blue:0.427f alpha:1.00f]];
            [labelThree setTextColor:[NSColor colorWithCalibratedRed:
                0.31f green:0.31f blue:0.31f alpha:1]];
            maxProgress = 0.37;
            currentView = [[StepThree alloc] initWithFrame:CGRectMake(0, 0,
                self.frame.size.width, self.frame.size.height * 0.25)];
            [(StepThree *)currentView setDelegate:self];
            [(StepThree *)currentView setRefineId:refineId];
            [self addSubview:currentView];
            break;
        case 4:
            [titleLabel setStringValue:@"Step Four"];
            [labelThree setTextColor:[NSColor colorWithCalibratedRed:
                0.204f green:0.322f blue:0.427f alpha:1.00f]];
            [labelSix setTextColor:[NSColor colorWithCalibratedRed:
                0.31f green:0.31f blue:0.31f alpha:1]];
            maxProgress = 0.68;
            currentView = [[StepFour alloc] initWithFrame:CGRectMake(0, 0,
                self.frame.size.width, self.frame.size.height * 0.25)];
            [(StepFour *)currentView setDelegate:self];
            [(StepFour *)currentView setRefineId:refineId];
            [self addSubview:currentView];
            break;
        case 5:
            [titleLabel setStringValue:@"Step Five"];
            [labelSix setTextColor:[NSColor colorWithCalibratedRed:
                0.204f green:0.322f blue:0.427f alpha:1.00f]];
            [labelFive setTextColor:[NSColor colorWithCalibratedRed:
                0.31f green:0.31f blue:0.31f alpha:1]];
            maxProgress = 0.79;
            currentView = [[StepFive alloc] initWithFrame:CGRectMake(0, 0,
                self.frame.size.width, self.frame.size.height * 0.25)];
            [(StepFive *)currentView setDelegate:self];
            [(StepFive *)currentView setRefineId:refineId];
            [self addSubview:currentView];
            break;
        case 6:
            [titleLabel setStringValue:@"Step Six"];
            [labelFive setTextColor:[NSColor colorWithCalibratedRed:
                0.204f green:0.322f blue:0.427f alpha:1.00f]];
            [labelFour setTextColor:[NSColor colorWithCalibratedRed:
                0.31f green:0.31f blue:0.31f alpha:1]];
            maxProgress = 0.92;
            currentView = [[StepSix alloc] initWithFrame:CGRectMake(0, 0,
                self.frame.size.width, self.frame.size.height * 0.25)];
            [(StepSix *)currentView setDelegate:self];
            [self addSubview:currentView];
            break;
        case 7:
            [titleLabel setStringValue:@"Life Cycle"];
            [progress setProgress:0.0 animated:YES];
            [timer invalidate];
            timer = [NSTimer scheduledTimerWithTimeInterval:.01 target:self
                selector:@selector(animateProgressView) userInfo:nil repeats:YES];
            maxProgress = 1;
            currentView = [[StepSeven alloc] initWithFrame:CGRectMake(0, 0,
                self.frame.size.width, self.frame.size.height * 0.25)];
            [(StepSeven *)currentView setDelegate:self];
            [self addSubview:currentView];
            break;
        default:
            break;
    }
}

// Delegate Optional Method
- (void)saveRefineProject:(NSString *)idRefine
{
    refineId = idRefine;
}

- (void)saveRefineMetadata:(NSMutableArray *)metRefine
{
    metaRefine = [[NSMutableArray alloc] initWithArray:metRefine];
}

// Delegate Required Method
- (void)showNextStep:(int)step
{
    if(currentView != nil)
    {
        [self clearCurrentView:step];
    }
    else
    {
        [self showInternalStep:step];
    }
}

@end
