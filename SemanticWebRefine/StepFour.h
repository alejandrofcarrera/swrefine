//
//  StepFour.h
//  SemanticWebRefine
//
//  Created by Alejandro Fdez Carrera on 12/12/14.
//  Copyright (c) 2014 Alejandro F. Carrera. All rights reserved.
//

#import <Cocoa/Cocoa.h>

#import "StepDelegate.h"

@interface StepFour : NSView <NSURLConnectionDelegate, NSURLConnectionDataDelegate>{
    
    NSTextField *fileInput;
    NSButton *fileButton;

    NSTimer *timer;
    
}

@property (nonatomic, weak) id<ShowStepDelegate> delegate;
@property (nonatomic, strong) NSString *refineId;

@end
