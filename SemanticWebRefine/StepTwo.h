//
//  StepTwo.h
//  SemanticWebRefine
//
//  Created by Alejandro Fdez Carrera on 12/12/14.
//  Copyright (c) 2014 Alejandro F. Carrera. All rights reserved.
//

#import <Cocoa/Cocoa.h>

#import "StepDelegate.h"
#import "GeneratorHTML.h"

@interface StepTwo : NSView <NSURLConnectionDelegate, NSURLConnectionDataDelegate>{
    
    NSTextField *informationLabel;
    
    NSTextField *columnDescription;
    NSTextField *columnDescriptionName;
    NSTextField *columnBlank;
    NSTextField *columnBlankValue;
    NSTextField *columnDuplicate;
    NSTextField *columnDuplicateValue;
    NSTextField *columnNumeric;
    NSTextField *columnNumericValue;
    
    NSTextField *row0Description;
    NSTextField *row0Value;
    
    NSButton *columnButton;
    
    NSMutableArray *columnsProject;
    
    NSAlert *alert;
    int currentColumn;
}

@property (nonatomic, weak) id<ShowStepDelegate> delegate;
@property (nonatomic, strong) NSString *refineId;

- (void)executeGetInformation;

@end
