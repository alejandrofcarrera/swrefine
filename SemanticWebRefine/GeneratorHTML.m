//
//  GeneratorHTML.m
//  SemanticWebRefine
//
//  Created by Alejandro Fdez Carrera on 15/12/14.
//  Copyright (c) 2014 Alejandro F. Carrera. All rights reserved.
//

#import "GeneratorHTML.h"

@implementation GeneratorHTML

+ (void)createHTML:(NSMutableArray *)values
{
    @autoreleasepool {
    NSString *head = @"<?xml version=\"1.0\" encoding=\"utf-8\"?>\
    <!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Strict//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd\">\
    <html xmlns=\"http://www.w3.org/1999/xhtml\" lang=\"en\" xml:lang=\"en\">\
    <head>\
    <title>analysis</title>\
    <meta http-equiv=\"Content-Type\" content=\"text/html;charset=utf-8\"/>\
    <meta name=\"title\" content=\"Analysis\"/>\
    <meta name=\"author\" content=\"Alejandro F. Carrera\"/>\
    <meta name=\"description\" content=\"Analysis created by SWR\"/>\
    <meta name=\"keywords\" content=\"\"/>\
    <style type=\"text/css\">\
    html { font-size: 12pt; }\
    .title  { text-align: center; }\
    .todo   { color: red; }\
    .done   { color: green; }\
    .tag    { background-color: #add8e6; font-weight:normal }\
    .timestamp { color: #bebebe; }\
    .timestamp-kwd { color: #5f9ea0; }\
    .right  { margin-left:auto; margin-right:0px;  text-align:right; }\
    .left   { margin-left:0px;  margin-right:auto; text-align:left; }\
    .center { margin-left:auto; margin-right:auto; text-align:center; }\
    .tg  { border-collapse:collapse;border-spacing:0;border-color:#ccc; }\
    .tg td{ font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:0px;overflow:hidden;word-break:normal;border-color:#ccc;color:#333;background-color:#fff; }\
    .tg th{ font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:0px;overflow:hidden;word-break:normal;border-color:#ccc;color:#333;background-color:#f0f0f0; }\
    .tg .tg-header{ text-align:center }\
    .tg .tg-blackl { text-align: left; color: black;}\
    .tg .tg-black { text-align: center; color: black;}\
    </style>\
    </head>";
    
    NSString *body = @"<body><div id=\"content\"><h1 class=\"title\">Analysis created by SWR</h1>\
    <table class=\"tg\" style=\"undefined;table-layout: fixed;\">\
    <colgroup><col style=\"width: 150px\"><col style=\"width: 80px\">\
    <col style=\"width: 200px\"><col style=\"width: 200px\"><col style=\"width: 200px\">\
    <col style=\"width: 200px\"><col style=\"width: 200px\"></colgroup><tr>\
    <th class=\"tg-header\">Column</th>\
    <th class=\"tg-header\">Blanks</th><th class=\"tg-header\">Duplicates</th>\
    <th class=\"tg-header\">Duplicate Number</th><th class=\"tg-header\">Type</th>\
    <th class=\"tg-header\">Details / Range</th><th class=\"tg-header\">Example CSV Value</th></tr>";
    
    for(int i = 0; i < values.count -1; i++)
    {
        NSDictionary *j = values[i];
        body = [body stringByAppendingString:@"<tr>"];
        body = [body stringByAppendingString:[NSString stringWithFormat:@"<td class=\"tg-blackl\">%@</td>",j[@"name"]]];
        
        NSString *bl = (j[@"blank"] == [NSNumber numberWithBool:YES]) ? @"true" : @"false";
        body = [body stringByAppendingString:[NSString stringWithFormat:@"<td class=\"tg-black\">%@</td>",bl]];
        
        NSString *du = (j[@"duplicate"] == [NSNumber numberWithBool:YES]) ? @"true" : @"false";
        body = [body stringByAppendingString:[NSString stringWithFormat:@"<td class=\"tg-black\">%@</td>",du]];
        
        NSString *duN = (j[@"duplicateNumber"]) ? [NSString stringWithFormat:@"%@", j[@"duplicateNumber"]] : @"";
        body = [body stringByAppendingString:[NSString stringWithFormat:@"<td class=\"tg-black\">%@</td>",duN]];
        
        NSString *ty = (j[@"numeric"] == [NSNumber numberWithBool:YES]) ? @"Number" : j[@"type"];
        body = [body stringByAppendingString:[NSString stringWithFormat:@"<td class=\"tg-black\">%@</td>",ty]];
        
        NSString *de = (j[@"range"]) ? j[@"range"] : @"";
        body = [body stringByAppendingString:[NSString stringWithFormat:@"<td class=\"tg-black\">%@</td>",de]];
        
        body = [body stringByAppendingString:[NSString stringWithFormat:@"<td class=\"tg-black\">%@</td>",values[values.count-1][i]]];
        body = [body stringByAppendingString:@"</tr>"];
    }
    
        body = [body stringByAppendingString:[NSString stringWithFormat:@"</table></div><div id=\"postamble\">\
        <p class=\"timestamp\">Date: %@</p></div></body></html>", [NSDate date]]];
        
        head = [head stringByAppendingString:body];
    
    
        NSData *data = [head dataUsingEncoding:NSUTF16StringEncoding];

        [[NSFileManager defaultManager] createFileAtPath:@"/tmp/Analysis.html" contents:data attributes:nil];
        
        NSLog(@"FileManager - Created /tmp/Analysis.html");

    }
    
}

@end

