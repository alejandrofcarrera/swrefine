//
//  StepFive.m
//  SemanticWebRefine
//
//  Created by Alejandro Fdez Carrera on 12/12/14.
//  Copyright (c) 2014 Alejandro F. Carrera. All rights reserved.
//

#import "StepFive.h"

@implementation StepFive

@synthesize refineId;

- (id)init
{
    if(self == nil)
    {
        self = [super init];
    }
    return self;
}

- (id)initWithFrame:(NSRect)frameRect
{
    self = [super initWithFrame:frameRect];
    
    [self addFileLabel];
    [self addFileInput];
    [self addFileButton];
    
    return self;
}

- (void)addFileLabel
{
    NSTextField *fileLabel = [[NSTextField alloc] initWithFrame:CGRectMake(
        self.frame.size.width * 0.08, self.frame.size.height * 0.33,
        self.frame.size.width * 0.45, self.frame.size.height * 0.3)];
    [fileLabel setBezeled:NO];
    [fileLabel setDrawsBackground:NO];
    [fileLabel setEditable:NO];
    [fileLabel setSelectable:NO];
    [fileLabel setTextColor:[NSColor colorWithDeviceRed:
        0.31f green:0.31f blue:0.31f alpha:1]];
    [fileLabel setFont:[NSFont fontWithName:@"Helvetica Bold" size:16.0f]];
    [fileLabel setStringValue:@"Type Commands:"];
    [self addSubview:fileLabel];
}

- (void)addFileInput
{
    fileInput = [[NSTextField alloc] initWithFrame:CGRectMake(
        self.frame.size.width * 0.32, self.frame.size.height * 0.4,
        self.frame.size.width * 0.48, self.frame.size.height * 0.25)];
    [fileInput setBezeled:YES];
    [fileInput setDrawsBackground:NO];
    [fileInput setEditable:YES];
    [fileInput setSelectable:YES];
    [fileInput setTextColor:[NSColor colorWithDeviceRed:
        0.31f green:0.31f blue:0.31f alpha:1]];
    [fileInput setFont:[NSFont fontWithName:@"Helvetica" size:14.0f]];
    [fileInput setPlaceholderString:@"Refine Commands at json format"];
    [self addSubview:fileInput];
}

- (void)addFileButton
{
    fileButton = [[NSButton alloc] initWithFrame:CGRectMake(
        self.frame.size.width * 0.82, self.frame.size.height * 0.41,
        self.frame.size.width * 0.15, self.frame.size.height * 0.25)];
    [fileButton setBordered:NO];
    [fileButton setAlignment:NSLeftTextAlignment];
    [fileButton setFont: [NSFont fontWithName:@"Helvetica" size:16.f]];
    [fileButton setTitle:@"Execute"];
    [fileButton setTarget:self];
    [fileButton setAction:@selector(clickButtonSelect:)];
    [self addSubview:fileButton];
}

- (IBAction)clickButtonSelect:(id)sender
{
    if(fileInput.stringValue.length > 0)
    {
        NSError *error;
        [NSJSONSerialization JSONObjectWithData:[fileInput.stringValue dataUsingEncoding:NSUTF16StringEncoding] options:0 error:&error];
        
        if(error)
        {
            [self showError:@"Invalid JSON (NSJSONSerialization)"];
        }
        else
        {
            [self applyProperties:fileInput.stringValue];
        }
    }
}

- (void)showError:(NSString *)type
{
    
    // Enable input and button
    [fileButton setEnabled:YES];
    [fileInput setEditable:YES];
    [fileInput setEnabled:YES];
    [fileInput setStringValue:@""];
    [fileInput setTextColor:[NSColor colorWithDeviceRed:
        0.31f green:0.31f blue:0.31f alpha:1]];
    
    // Alert with error
    NSAlert *alert = [[NSAlert alloc] init];
    [alert setMessageText:@"Error - SWR"];
    [alert setInformativeText:type];
    [alert addButtonWithTitle:@"OK"];
    [alert runModal];
    
}

- (void)applyProperties:(NSString *)properties
{
    NSLog(@"--------------------");
    NSLog(@"Applying Properties:");
    
    // Block UI
    [fileButton setEnabled:NO];
    [fileInput setEditable:NO];
    [fileInput setEnabled:NO];
    [fileInput setTextColor:[NSColor lightGrayColor]];
    
    // Please wait label
    [fileButton setTitle:@"Please wait"];
    
    // Apply Properties to Refine Project
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:
        [NSURL URLWithString:[NSString stringWithFormat:@"http://127.0.0.1:3333/command/core/apply-operations?project=%@", refineId]]];
    
    [request setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
    [request setHTTPShouldHandleCookies:NO];
    [request setTimeoutInterval:300];
    [request setHTTPMethod:@"POST"];
    
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField: @"Content-Type"];
    
    NSString *strUTF8 = (NSString *)CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(kCFAllocatorDefault, (CFStringRef)properties, NULL, CFSTR(":/?#[]@!$&’()*+,;="), kCFStringEncodingUTF8));
    
    [request setHTTPBody:[[NSString stringWithFormat:@"operations=%@&engine={\"facets\":[],\"mode\":\"row-based\"}", strUTF8]dataUsingEncoding:NSUTF8StringEncoding]];
    
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue currentQueue]
        completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
        if(data.length > 0)
        {
            NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
            NSLog(@"Done (Properties) - HTTP Status code (%d)", (int) httpResponse.statusCode);
                                   
            // Export Turtle File
            NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:
                [NSURL URLWithString:[NSString stringWithFormat:@"http://localhost:3333/command/core/export-rows/%@.ttl", refineId]]];
                                   
            [request setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
            [request setHTTPShouldHandleCookies:NO];
            [request setTimeoutInterval:300];
            [request setHTTPMethod:@"POST"];
                                   
            [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField: @"Content-Type"];
                                   
            [request setHTTPBody:[[NSString stringWithFormat:@"engine={\"facets\":[],\"mode\":\"row-based\"}&project=%@&format=Turtle", refineId]dataUsingEncoding:NSUTF8StringEncoding]];
                                   
            [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue currentQueue]
                completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
                if(data.length > 0)
                {
                                                                  
                    NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                    NSLog(@"Done (Export) - HTTP Status code (%d)", (int) httpResponse.statusCode);
                                                                  
                    [[NSFileManager defaultManager] createFileAtPath:@"/tmp/Export-updated.ttl" contents:data attributes:nil];
                                                                  
                    NSLog(@"FileManager - Created /tmp/Export-updated.ttl");
                                                                  
                    [self.delegate showNextStep:6];
                }
                else
                {
                    [self showError:@"Check RDF Extension"];
                }
            }];
        }
        else
        {
            [self showError:@"Check Refine Endpoint: 127.0.0.1:3333"];
        }
    }];
}

@end
