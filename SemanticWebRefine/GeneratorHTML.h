//
//  GeneratorHTML.h
//  SemanticWebRefine
//
//  Created by Alejandro Fdez Carrera on 15/12/14.
//  Copyright (c) 2014 Alejandro F. Carrera. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GeneratorHTML : NSObject

+ (void)createHTML:(NSMutableArray *)values;

@end
