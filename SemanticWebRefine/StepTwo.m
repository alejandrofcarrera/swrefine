//
//  StepTwo.m
//  SemanticWebRefine
//
//  Created by Alejandro Fdez Carrera on 12/12/14.
//  Copyright (c) 2014 Alejandro F. Carrera. All rights reserved.
//

#import "StepTwo.h"

@implementation StepTwo

@synthesize refineId;

- (id)init
{
    if(self == nil)
    {
        self = [super init];
    }
    return self;
}

- (id)initWithFrame:(NSRect)frameRect
{
    self = [super initWithFrame:frameRect];
    
    columnDescription = nil;
    columnDescriptionName = nil;
    columnBlank = nil;
    columnBlankValue = nil;
    columnDuplicate = nil;
    columnDuplicateValue = nil;
    columnNumeric = nil;
    columnNumericValue = nil;
    
    columnButton = nil;
    
    [self addInformationLabel];
    
    return self;
}

- (void)addInformationLabel
{
    informationLabel = [[NSTextField alloc] initWithFrame:CGRectMake(
        0, self.frame.size.height * 0.4,
        self.frame.size.width, self.frame.size.height * 0.3)];
    [informationLabel setBezeled:NO];
    [informationLabel setDrawsBackground:NO];
    [informationLabel setEditable:NO];
    [informationLabel setSelectable:NO];
    [informationLabel setTextColor:[NSColor lightGrayColor]];
    [informationLabel setAlignment:NSCenterTextAlignment];
    [informationLabel setFont:[NSFont fontWithName:@"Helvetica Bold" size:16.0f]];
    [informationLabel setStringValue:@"Getting information ... Please wait"];
    [self addSubview:informationLabel];
}

- (void)executeGetInformation
{
    // Get Refine Projects
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:
        [NSURL URLWithString:[NSString stringWithFormat:@"http://localhost:3333/command/core/get-models?project=%@",
        refineId]]];
    
    [request setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
    [request setHTTPShouldHandleCookies:NO];
    [request setTimeoutInterval:300];
    [request setHTTPMethod:@"GET"];
    
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue currentQueue]
        completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
        if(data.length > 0)
        {
            NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
            NSArray *jsonV = json[@"columnModel"][@"columns"];
            columnsProject = [[NSMutableArray alloc] init];
            for(int i = 0; i < jsonV.count; i++)
            {
                [columnsProject addObject:[[NSMutableDictionary alloc] initWithObjects:
                     @[jsonV[i][@"name"]] forKeys:@[@"name"]]];
            }
            [columnsProject addObject:[[NSMutableArray alloc] init]];
            for(int i = 0; i < jsonV.count; i++)
            {
                [columnsProject[columnsProject.count -1]addObject:@"null"];
            }
            json = nil;
            jsonV = nil;
            
            currentColumn = 0;
            
            NSLog(@"--------------------");
            NSLog(@"Getting Information:");
            [self getBlankPropertiesColumn:columnsProject[currentColumn][@"name"]];
        }
        else
        {
            [self showError:[NSString stringWithFormat:@"Error to get Model for project %@", refineId]];
        }
    }];
}

- (void)showInformationAbout:(NSString *)column
{
    if(informationLabel != nil)
    {
        [informationLabel removeFromSuperview];
        informationLabel = nil;
    }

    columnDescription = [[NSTextField alloc] initWithFrame:CGRectMake(
        self.frame.size.width * 0.1, self.frame.size.height * 0.65,
        self.frame.size.width * 0.2, self.frame.size.height * 0.3)];
    [columnDescription setBezeled:NO];
    [columnDescription setDrawsBackground:NO];
    [columnDescription setEditable:NO];
    [columnDescription setSelectable:NO];
    [columnDescription setTextColor:[NSColor grayColor]];
    [columnDescription setFont:[NSFont fontWithName:@"Helvetica Bold" size:16.0f]];
    [columnDescription setStringValue:@"Column name:"];
    [self addSubview:columnDescription];
    
    columnDescriptionName = [[NSTextField alloc] initWithFrame:CGRectMake(
        self.frame.size.width * 0.295, self.frame.size.height * 0.64,
        self.frame.size.width * 0.3, self.frame.size.height * 0.3)];
    [columnDescriptionName setBezeled:NO];
    [columnDescriptionName setDrawsBackground:NO];
    [columnDescriptionName setEditable:NO];
    [columnDescriptionName setSelectable:NO];
    [columnDescriptionName setTextColor:[NSColor grayColor]];
    [columnDescriptionName setFont:[NSFont fontWithName:@"Helvetica" size:15.0f]];
    [self addSubview:columnDescriptionName];
    NSString *cValue = [NSString stringWithString:column];
    if(cValue.length > 12){
        cValue = [cValue substringToIndex:12];
        cValue = [cValue stringByAppendingString:@"..."];
    }
    [columnDescriptionName setStringValue:cValue];
    cValue = nil;
    
    columnNumeric = [[NSTextField alloc] initWithFrame:CGRectMake(
        self.frame.size.width * 0.1, self.frame.size.height * 0.35,
        self.frame.size.width * 0.2, self.frame.size.height * 0.3)];
    [columnNumeric setBezeled:NO];
    [columnNumeric setDrawsBackground:NO];
    [columnNumeric setEditable:NO];
    [columnNumeric setSelectable:NO];
    [columnNumeric setTextColor:[NSColor grayColor]];
    [columnNumeric setFont:[NSFont fontWithName:@"Helvetica Bold" size:16.0f]];
    [columnNumeric setStringValue:@"isNumeric:"];
    [self addSubview:columnNumeric];
    
    columnNumericValue = [[NSTextField alloc] initWithFrame:CGRectMake(
        self.frame.size.width * 0.245, self.frame.size.height * 0.34,
        self.frame.size.width * 0.4, self.frame.size.height * 0.3)];
    [columnNumericValue setBezeled:NO];
    [columnNumericValue setDrawsBackground:NO];
    [columnNumericValue setEditable:NO];
    [columnNumericValue setSelectable:NO];
    [columnNumericValue setTextColor:[NSColor grayColor]];
    [columnNumericValue setFont:[NSFont fontWithName:@"Helvetica" size:15.0f]];
    [self addSubview:columnNumericValue];
    NSString *nValue = (columnsProject[currentColumn][@"numeric"] == [NSNumber numberWithBool:YES])
         ? @"Yes" : [NSString stringWithFormat:@"No, maybe %@", columnsProject[currentColumn][@"type"]];
    if(columnsProject[currentColumn][@"numeric"] == [NSNumber numberWithBool:YES])
    {
        nValue = [nValue stringByAppendingString:[NSString stringWithFormat:@", Range %@",
            columnsProject[currentColumn][@"range"]]];
    }
    [columnNumericValue setStringValue:nValue];
    nValue = nil;
    
    row0Description = [[NSTextField alloc] initWithFrame:CGRectMake(
        self.frame.size.width * 0.1, self.frame.size.height * 0.05,
        self.frame.size.width * 0.3, self.frame.size.height * 0.3)];
    [row0Description setBezeled:NO];
    [row0Description setDrawsBackground:NO];
    [row0Description setEditable:NO];
    [row0Description setSelectable:NO];
    [row0Description setTextColor:[NSColor grayColor]];
    [row0Description setFont:[NSFont fontWithName:@"Helvetica Bold" size:16.0f]];
    [row0Description setStringValue:@"CSV nonBlank value:"];
    [self addSubview:row0Description];
    
    row0Value = [[NSTextField alloc] initWithFrame:CGRectMake(
        self.frame.size.width * 0.375, self.frame.size.height * 0.03,
        self.frame.size.width * 0.4, self.frame.size.height * 0.3)];
    [row0Value setBezeled:NO];
    [row0Value setDrawsBackground:NO];
    [row0Value setEditable:NO];
    [row0Value setSelectable:NO];
    [row0Value setTextColor:[NSColor grayColor]];
    [row0Value setFont:[NSFont fontWithName:@"Helvetica" size:14.0f]];
    if([columnsProject[columnsProject.count - 1][currentColumn] isEqualToString:@"null"])
    {
        [row0Value setStringValue:@"Not found"];
    }
    else
    {
        if([columnsProject[columnsProject.count -1][currentColumn] length] > 24)
        {
            NSString *rValue = columnsProject[columnsProject.count - 1][currentColumn];
            rValue = [rValue substringToIndex:24];
            rValue = [rValue stringByAppendingString:@"..."];
            [row0Value setStringValue:rValue];
            rValue = nil;
        }
        else
        {
            [row0Value setStringValue:columnsProject[columnsProject.count - 1][currentColumn]];
        }
    }
    [self addSubview:row0Value];
    
    columnBlank = [[NSTextField alloc] initWithFrame:CGRectMake(
        self.frame.size.width * 0.5, self.frame.size.height * 0.65,
        self.frame.size.width * 0.2, self.frame.size.height * 0.3)];
    [columnBlank setBezeled:NO];
    [columnBlank setDrawsBackground:NO];
    [columnBlank setEditable:NO];
    [columnBlank setSelectable:NO];
    [columnBlank setTextColor:[NSColor grayColor]];
    [columnBlank setFont:[NSFont fontWithName:@"Helvetica Bold" size:16.0f]];
    [columnBlank setStringValue:@"Blank:"];
    [self addSubview:columnBlank];
    
    columnBlankValue = [[NSTextField alloc] initWithFrame:CGRectMake(
        self.frame.size.width * 0.585, self.frame.size.height * 0.64,
        self.frame.size.width * 0.2, self.frame.size.height * 0.3)];
    [columnBlankValue setBezeled:NO];
    [columnBlankValue setDrawsBackground:NO];
    [columnBlankValue setEditable:NO];
    [columnBlankValue setSelectable:NO];
    [columnBlankValue setTextColor:[NSColor grayColor]];
    [columnBlankValue setFont:[NSFont fontWithName:@"Helvetica" size:15.0f]];
    [self addSubview:columnBlankValue];
    NSString *bValue = (columnsProject[currentColumn][@"blank"] == [NSNumber numberWithBool:YES])
        ? @"Yes" : @"No";
    [columnBlankValue setStringValue:bValue];
    bValue = nil;
    
    columnDuplicate = [[NSTextField alloc] initWithFrame:CGRectMake(
        self.frame.size.width * 0.67, self.frame.size.height * 0.65,
        self.frame.size.width * 0.2, self.frame.size.height * 0.3)];
    [columnDuplicate setBezeled:NO];
    [columnDuplicate setDrawsBackground:NO];
    [columnDuplicate setEditable:NO];
    [columnDuplicate setSelectable:NO];
    [columnDuplicate setTextColor:[NSColor grayColor]];
    [columnDuplicate setFont:[NSFont fontWithName:@"Helvetica Bold" size:16.0f]];
    [columnDuplicate setStringValue:@"Duplicate:"];
    [self addSubview:columnDuplicate];

    columnDuplicateValue = [[NSTextField alloc] initWithFrame:CGRectMake(
        self.frame.size.width * 0.805, self.frame.size.height * 0.64,
        self.frame.size.width * 0.2, self.frame.size.height * 0.3)];
    [columnDuplicateValue setBezeled:NO];
    [columnDuplicateValue setDrawsBackground:NO];
    [columnDuplicateValue setEditable:NO];
    [columnDuplicateValue setSelectable:NO];
    [columnDuplicateValue setTextColor:[NSColor grayColor]];
    [columnDuplicateValue setFont:[NSFont fontWithName:@"Helvetica" size:15.0f]];
    [self addSubview:columnDuplicateValue];
    NSString *dValue = (columnsProject[currentColumn][@"duplicate"] == [NSNumber numberWithBool:YES])
        ? @"Yes" : @"No";
    if(columnsProject[currentColumn][@"duplicate"] == [NSNumber numberWithBool:YES])
    {
        dValue = [dValue stringByAppendingString: [NSString stringWithFormat:@" (%@)", columnsProject[currentColumn][@"duplicateNumber"]]];
    }
    [columnDuplicateValue setStringValue:dValue];
    dValue = nil;
    
    [self addColumnButton];
    
}

- (void)cleanInformation
{
    [columnDescription removeFromSuperview];
    [columnDescriptionName removeFromSuperview];
    [columnBlank removeFromSuperview];
    [columnBlankValue removeFromSuperview];
    [columnDuplicate removeFromSuperview];
    [columnDuplicateValue removeFromSuperview];
    [columnNumeric removeFromSuperview];
    [columnNumericValue removeFromSuperview];
    [columnButton removeFromSuperview];
    [row0Description removeFromSuperview];
    [row0Value removeFromSuperview];
    
    columnDescription = nil;
    columnDescriptionName = nil;
    columnBlank = nil;
    columnBlankValue = nil;
    columnDuplicate = nil;
    columnDuplicateValue = nil;
    columnNumeric = nil;
    columnNumericValue = nil;
    columnButton = nil;
    row0Description = nil;
    row0Value = nil;
    
    [self addInformationLabel];
    
}

- (void)getBlankPropertiesColumn:(NSString *)column
{

    // Get Column Blank Properties
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:
        [NSURL URLWithString:[NSString stringWithFormat:
        @"http://localhost:3333/command/core/compute-facets?project=%@",
        refineId]]];
    
    [request setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
    [request setHTTPShouldHandleCookies:NO];
    [request setTimeoutInterval:300];
    [request setHTTPMethod:@"POST"];
    
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField: @"Content-Type"];
    
    [request setHTTPBody:[[NSString stringWithFormat:@"engine={\"facets\":[{\"type\":\"list\",\"name\":\"%@\",\"columnName\":\"%@\",\"expression\":\"isBlank(value)\",\"omitBlank\":false,\"omitError\":true,\"selection\":[],\"selectBlank\":true,\"selectError\":false,\"invert\":false}],\"mode\":\"row-based\"}", column, column]
        dataUsingEncoding:NSUTF8StringEncoding]];
    
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue currentQueue]
        completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
        if(data.length > 0)
        {
            NSLog(@"Column[%@] - getBlanks", column);
            NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
            if([json[@"facets"][0][@"choices"] count] > 1)
            {
                if([json[@"facets"][0][@"choices"][0][@"v"][@"l"] isEqualToString:@"true"])
                {
                    columnsProject[currentColumn][@"blank"] = ([json[@"facets"][0][@"choices"][0][@"c"] intValue] > 0) ?
                    [NSNumber numberWithBool:YES] : [NSNumber numberWithBool:NO];
                }
                else
                {
                    columnsProject[currentColumn][@"blank"] = ([json[@"facets"][0][@"choices"][1][@"c"] intValue] > 0) ?
                    [NSNumber numberWithBool:YES] : [NSNumber numberWithBool:NO];
                }
            }
            else
            {
                if([json[@"facets"][0][@"choices"][0][@"v"][@"l"] isEqualToString:@"true"])
                {
                    columnsProject[currentColumn][@"blank"] = [NSNumber numberWithBool:YES];
                }
                else
                {
                    columnsProject[currentColumn][@"blank"] = [NSNumber numberWithBool:NO];
                }
            }
            
            json = nil;
            
            [self getValueProperties:column];
        }
        else
        {
            [self showError:[NSString stringWithFormat:@"Error to get model for property %@", column]];
        }
    }];
}

- (void)getValueProperties:(NSString *)column
{
    if([columnsProject[columnsProject.count-1][currentColumn] isEqualToString:@"null"])
    {
        // Get Column Non Blank Value
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:
            [NSURL URLWithString:[NSString stringWithFormat:
            @"http://localhost:3333/command/core/get-rows?project=%@&start=0&limit=1",
            refineId]]];

        [request setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
        [request setHTTPShouldHandleCookies:NO];
        [request setTimeoutInterval:300];
        [request setHTTPMethod:@"POST"];
        
        [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField: @"Content-Type"];
        
        [request setHTTPBody:[[NSString stringWithFormat:@"engine={\"facets\":[{\"type\":\"list\",\"name\":\"%@\",\"columnName\":\"%@\",\"expression\":\"isBlank(value)\",\"omitBlank\":false,\"omitError\":true,\"selection\":[{\"v\":{\"v\":false,\"l\":\"false\"}}],\"selectBlank\":false,\"selectError\":false,\"invert\":false}],\"mode\":\"row-based\"}", column, column] dataUsingEncoding:NSUTF8StringEncoding]];
        
        [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue currentQueue]
            completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
            if(data.length > 0)
            {
                NSLog(@"Column[%@] - getValueBlanks", column);
                NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                NSObject *v = json[@"rows"][0][@"cells"][currentColumn];
                if([v isKindOfClass:[NSDictionary class]])
                {
                    NSDictionary *vD = (NSDictionary *)v;
                    columnsProject[columnsProject.count - 1][currentColumn] =
                        [NSString stringWithFormat:@"%@",vD[@"v"]];
                    vD = nil;
                }
                v = nil;
                json = nil;
                                       
                [self getDuplicateProperties:column];
            }
            else
            {
                [self showError:[NSString stringWithFormat:@"Error to get non blank for property %@", column]];
            }
        }];
    }
    else
    {
        [self getDuplicateProperties:column];
    }
}

- (void)getDuplicateProperties:(NSString *)column
{
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:
        [NSURL URLWithString:[NSString stringWithFormat:
        @"http://localhost:3333/command/core/compute-facets?project=%@",
        refineId]]];
    
    [request setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
    [request setHTTPShouldHandleCookies:NO];
    [request setTimeoutInterval:300];
    [request setHTTPMethod:@"POST"];
    
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField: @"Content-Type"];
    
    [request setHTTPBody:[[NSString stringWithFormat:@"engine={\"facets\":[{\"type\":\"list\",\"name\":\"%@\",\"columnName\":\"%@\",\"expression\":\"facetCount(value, 'value', '%@') > 1\",\"omitBlank\":true,\"omitError\":true,\"selection\":[],\"selectBlank\":false,\"selectError\":false,\"invert\":false}],\"mode\":\"row-based\"}", column, column, column] dataUsingEncoding:NSUTF8StringEncoding]];
    
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue currentQueue]
        completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
        if(data.length > 0)
        {
            NSLog(@"Column[%@] - getDuplicates", column);
            NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
            if([json[@"facets"][0][@"choices"] count] > 1)
            {
                if([json[@"facets"][0][@"choices"][0][@"v"][@"l"] isEqualToString:@"true"])
                {
                    columnsProject[currentColumn][@"duplicate"] = ([json[@"facets"][0][@"choices"][0]
                        [@"c"] intValue] > 0) ? [NSNumber numberWithBool:YES] : [NSNumber numberWithBool:NO];
                    if(columnsProject[currentColumn][@"duplicate"] == [NSNumber numberWithBool:YES])
                    {
                        columnsProject[currentColumn][@"duplicateNumber"] = json[@"facets"][0][@"choices"][0][@"c"];
                    }
                }
                else
                {
                    columnsProject[currentColumn][@"duplicate"] = ([json[@"facets"][0][@"choices"][1]
                        [@"c"] intValue] > 0) ? [NSNumber numberWithBool:YES] : [NSNumber numberWithBool:NO];
                    if(columnsProject[currentColumn][@"duplicate"] == [NSNumber numberWithBool:YES])
                    {
                        columnsProject[currentColumn][@"duplicateNumber"] = json[@"facets"][0][@"choices"][1][@"c"];
                    }
                }
            }
            else
            {
                if([json[@"facets"][0][@"choices"][0][@"v"][@"l"] isEqualToString:@"false"])
                {
                    columnsProject[currentColumn][@"duplicate"] = [NSNumber numberWithBool:NO];
                }
                else
                {
                    columnsProject[currentColumn][@"duplicate"] = [NSNumber numberWithBool:YES];
                    columnsProject[currentColumn][@"duplicateNumber"] = json[@"facets"][0][@"choices"][0][@"c"];
                }
            }
            
            json = nil;
            [self getTypeProperties:column];
        }
        else
        {
            [self showError:[NSString stringWithFormat:@"Error to get model for property %@", column]];
        }
    }];
}

- (void)getTypeProperties:(NSString *)column
{
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:
        [NSURL URLWithString:[NSString stringWithFormat:
        @"http://localhost:3333/command/core/compute-facets?project=%@",
        refineId]]];
    
    [request setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
    [request setHTTPShouldHandleCookies:NO];
    [request setTimeoutInterval:300];
    [request setHTTPMethod:@"POST"];
    
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField: @"Content-Type"];
    
    [request setHTTPBody:[[NSString stringWithFormat:@"engine={\"facets\":[{\"type\":\"range\",\"name\":\"%@\",\"expression\":\"value\",\"columnName\":\"%@\",\"selectNumeric\":true,\"selectNonNumeric\":true,\"selectBlank\":false,\"selectError\":false}],\"mode\":\"row-based\"}", column, column] dataUsingEncoding:NSUTF8StringEncoding]];

    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue currentQueue]
        completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
        if(data.length > 0)
        {
            NSLog(@"Column[%@] - getNumericType", column);
            NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
            columnsProject[currentColumn][@"numeric"] = [json[@"facets"][0][@"error"]
                isEqualToString:@"No numeric value present."] ?
                [NSNumber numberWithBool:NO] : [NSNumber numberWithBool:YES];
                                   
            if(columnsProject[currentColumn][@"numeric"] == [NSNumber numberWithBool:NO])
            {
                json = nil;
                
                // Get Column Type Inference Properties
                NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:
                    [NSURL URLWithString:[NSString stringWithFormat:
                    @"http://localhost:3333/command/core/compute-facets?project=%@",
                    refineId]]];
                                       
                [request setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
                [request setHTTPShouldHandleCookies:NO];
                [request setTimeoutInterval:300];
                [request setHTTPMethod:@"POST"];
                                       
                [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField: @"Content-Type"];
                                       
                [request setHTTPBody:[[NSString stringWithFormat:@"engine={\"facets\":[{\"type\":\"list\",\"name\":\"%@\",\"columnName\":\"%@\",\"expression\":\"value\",\"omitBlank\":true,\"omitError\":true,\"selection\":[],\"selectBlank\":false,\"selectError\":false,\"invert\":false}],\"mode\":\"row-based\"}", column, column]dataUsingEncoding:NSUTF8StringEncoding]];

                [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue currentQueue]
                    completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
                    if(data.length > 0)
                    {
                        NSLog(@"Column[%@] - getSpecificType", column);
                        NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                        columnsProject[currentColumn][@"type"] = [json[@"facets"][0][@"choices"] count] == 2 ?
                            @"Boolean" : @"String or Date";
                        json = nil;
                                                                      
                        [self showInformationAbout: column];
                    }
                    else
                    {
                        [self showError:[NSString stringWithFormat:@"Error to get model for property %@", column]];
                    }
                }];
            }
            else
            {
                columnsProject[currentColumn][@"range"] = [NSString stringWithFormat:@"%@ - %@", json[@"facets"][0][@"min"], json[@"facets"][0][@"max"]];
                json = nil;
                [self showInformationAbout: column];
            }
        }
        else
        {
            [self showError:[NSString stringWithFormat:@"Error to get model for property %@", column]];
        }
    }];
}

- (void)addColumnButton
{
    columnButton = [[NSButton alloc] initWithFrame:CGRectMake(
        self.frame.size.width * 0.71, self.frame.size.height * 0.25,
        self.frame.size.width * 0.2, self.frame.size.height * 0.3)];
    [columnButton setBordered:NO];
    [columnButton setAlignment:NSLeftTextAlignment];
    [columnButton setFont: [NSFont fontWithName:@"Helvetica" size:16.f]];
    if(currentColumn < columnsProject.count - 2)
    {
        [columnButton setTitle:@"Next Column"];
    }
    else
    {
        [columnButton setTitle:@"Next Step"];
    }
    [columnButton setTarget:self];
    [columnButton setAction:@selector(clickButtonSelect:)];
    [self addSubview:columnButton];
}

- (IBAction)clickButtonSelect:(id)sender
{
    if(currentColumn < columnsProject.count - 1)
    {
        if(columnsProject[currentColumn][@"numeric"] == [NSNumber numberWithBool:NO])
        {
            if([columnsProject[currentColumn][@"type"] isEqualToString:@"String or Date"])
            {
                [self showChooseType];
            }
            else
            {
                [self showNextColumn];
            }
        }
        else
        {
            [self showNextColumn];
        }
    }
    else
    {
        [self showNextColumn];
    }
}

- (void)showNextColumn
{
    currentColumn++;
    if(currentColumn == columnsProject.count - 1)
    {
        [GeneratorHTML createHTML:columnsProject];
        [self.delegate saveRefineMetadata:columnsProject];
        [self.delegate showNextStep:3];
    }
    else
    {
        [self cleanInformation];
        [self getBlankPropertiesColumn:columnsProject[currentColumn][@"name"]];
    }
}

- (void)showChooseType
{
    alert = [[NSAlert alloc] init];
    [alert setMessageText:@"Inference Type - SWR"];
    [alert setInformativeText:@"Choose type for analysis"];
    NSButton *sAlert = [alert addButtonWithTitle:@"String"];
    [sAlert setTarget:self];
    [sAlert setAction:@selector(chooseTypeString)];
    NSButton *dAlert = [alert addButtonWithTitle:@"Date"];
    [dAlert setTarget:self];
    [dAlert setAction:@selector(chooseTypeDate)];
    [alert runModal];
}

- (void)chooseTypeString
{
    columnsProject[currentColumn][@"type"] = @"String";
    [self showNextColumn];
    [NSApp endSheet:[alert window]];
    alert = nil;
}

- (void)chooseTypeDate
{
    columnsProject[currentColumn][@"type"] = @"Date";
    [self showNextColumn];
    [NSApp endSheet:[alert window]];
    alert = nil;
}

- (void)showError:(NSString *)type
{
    
    // Alert with error
    NSAlert *alertE = [[NSAlert alloc] init];
    [alertE setMessageText:@"Error - SWR"];
    [alertE setInformativeText:type];
    [alertE addButtonWithTitle:@"OK"];
    [alertE runModal];
    
}

@end
