//
//  StepSeven.h
//  SemanticWebRefine
//
//  Created by Alejandro Fdez Carrera on 12/12/14.
//  Copyright (c) 2014 Alejandro F. Carrera. All rights reserved.
//

#import <Cocoa/Cocoa.h>

#import "StepDelegate.h"

@interface StepSeven : NSView {
    
    NSTextField *fileLabel;
    NSButton *fileButton;
    
}

@property (nonatomic, weak) id<ShowStepDelegate> delegate;

@end
