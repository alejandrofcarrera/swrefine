//
//  StepDelegate.h
//  SemanticWebRefine
//
//  Created by Alejandro Fdez Carrera on 12/12/14.
//  Copyright (c) 2014 Alejandro F. Carrera. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol ShowStepDelegate <NSObject>
@optional
- (void)saveRefineProject:(NSString *)idRefine;
- (void)saveRefineMetadata:(NSMutableArray *)metRefine;
@required
- (void)showNextStep:(int)step;
@end

@interface StepDelegate : NSObject

@end
