//
//  StepSix.h
//  SemanticWebRefine
//
//  Created by Alejandro Fdez Carrera on 12/12/14.
//  Copyright (c) 2014 Alejandro F. Carrera. All rights reserved.
//

#import <Cocoa/Cocoa.h>

#import "StepDelegate.h"

@interface StepSix : NSView {
    
    NSTextField *fileLabel;
    NSTextField *fileInput;
    NSButton *fileButton;
    
    int currentStep;
    
    NSString *turtle;
    
    NSAlert *alert;
    
}

@property (nonatomic, weak) id<ShowStepDelegate> delegate;

@end
