//
//  StepOne.h
//  SemanticWebRefine
//
//  Created by Alejandro Fdez Carrera on 12/12/14.
//  Copyright (c) 2014 Alejandro F. Carrera. All rights reserved.
//

#import <Cocoa/Cocoa.h>

#import "StepDelegate.h"

@interface StepOne : NSView <NSURLConnectionDelegate, NSURLConnectionDataDelegate>{
    
    NSTextField *fileInput;
    NSButton *fileButton;
    
    NSURLConnection *connection;
    NSMutableData *connectionData;
    
    NSString *projectName;
    
    int expectedData;
}

@property (nonatomic, weak) id<ShowStepDelegate> delegate;

@end
